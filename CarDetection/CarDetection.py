'''
Description: CarDetection.py is the TopLevel for image analysis for Pi Camera feed. It calls functionality from carSpots.py to run detection and has code to control the setting up of carRegions.
Author: Caleb Stott
Revision: 16/10/2015
'''
import numpy as np
import time
import cv2
#import pickle
#import math
from carSpot import *

#setup Camera
from picamera.array import PiRGBArray
from picamera import PiCamera
camera = PiCamera()
#camera.resolution = (2592, 1944)#full sensor
#camera.resolution = (1920, 1080)#!!Crops Sensor!! (see picamera.readthedocs.org/en/latest/fov.html)
camera.resolution = (1280,720)#Rest are Scaled Full sensor
#camera.resolution = (640,480)
#camera.resolution = (320,240)
camera.framerate = 15
rawCapture=PiRGBArray(camera, size=camera.resolution)

cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Frame', camera.resolution[0], camera.resolution[1])
cv2.setMouseCallback("Frame", carSpot.on_mouse)

#add spots (hard coded for now)
for i in range(439,444):#R211 Spots
    carSpot(carSpot.getNewSpotLocation(),takenThreshold=10,carSpotID=i,isTaken=False,
                                imageWidth=camera.resolution[0], imageHeight=camera.resolution[1])#inittialise image size in carSpot class in order to create masks

#for i in range(403,408):#R209 Spots
    #print (carSpot.getNewSpotLocation())
#    carSpot(carSpot.getNewSpotLocation(),takenThreshold=10,carSpotID=i)
#print (carSpot.spots)

carSpot.createTrackbars()

pauseDetection=False

#Main Program loop
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    #wait for camera to be ready
    time.sleep(0.1)

    #get image from camera
    image=frame.array

    #run algorithms and update spots
    if(not pauseDetection):
        carSpot.analyseSpots(image)
        carSpot.updateSpots()
    #draw state of spots on image
    image=carSpot.drawSpots(image)#draw outline indicating status of carSpot
    #display image
    cv2.imshow('Frame', image)

    #clear camera buffer
    rawCapture.truncate(0)

    #get keyboard input
    key=cv2.waitKey(20) & 0xFF

    if key==ord("q"):
        cv2.destroyAllWindows()
        break
    elif key==ord("p"):
        #toggle running the detection algorithms
        #allows for better performance when setting up the spots
        pauseDetection=not pauseDetection
    elif key==ord("2"):
        carSpot.moveAll(0,10)
    elif key==ord("a"):
        carSpot.addPrompt()
    elif key==ord("6"):
        carSpot.moveAll(10,0)
    elif key==ord("8"):
        carSpot.moveAll(0,-10)
    elif key==ord("4"):
        carSpot.moveAll(-10,0)
    elif key==ord("s"):
        carSpot.storeSpots('./spotSaves/'+str(camera.resolution[0])+'x'+str(camera.resolution[1])+'.'+SpotsFileName)
    elif key==ord("l"):
        carSpot.loadSpots('./spotSaves/'+str(camera.resolution[0])+'x'+str(camera.resolution[1])+'.'+SpotsFileName)
    #elif key==ord("w"):
    #elif key==ord("o"):

cv2.destroyAllWindows()
