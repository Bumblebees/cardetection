''' 
Description: tests.py work in progress, file is being built for the purpose of testing the detection algorithims against various resources.
Author: Caleb Stott
Revision: 14/10/2015
'''

from carSpot import *
import cv2
import numpy as np
import pytest
import time

'''
def func(x):
    return x+1

def test_func():
    #assert func(3)==5
    assert func(3)==4
'''



'''
carspot(self, pts, carSpotID, takenThreshold,
     colorDifferenceThreshold=defaultColorDifferenceThreshold,
     edgeThreshold=defaultEdgeThreshold,
     isTaken=False, imageWidth=0, imageHeight=0)
 '''
def BG312_image_1():
    #load image to test
    cap = cv2.VideoCapture('./testSamples/BG312_image_1.png')
    #set image size
    carSpot.imageWidth=int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    carSpot.imageHeight=int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    #define where the car spots are
    carSpot(pts=np.array([[538,738],[524,757],[550,760],[557,740]]),carSpotID=1,takenThreshold=0,isTaken=False)
    carSpot(pts=np.array([[544,710],[543,718],[560,717],[561,710]]),carSpotID=2,takenThreshold=0,isTaken=False)

    ret, image = cap.read()
    carSpot.analyseSpots(image)
    carSpot.updateSpots()
    '''
    #DEBUG
    image=carSpot.drawSpots(image)
    cv2.namedWindow('Frame', cv2.WINDOW_AUTOSIZE)
    cv2.imshow('Frame', image)
    time.sleep(5)
    '''
    return carSpot.spots

def test_BG312_image_1():
    spots=BG312_image_1()
    assert spots[0].detectionCount != 0 #taken spot
    assert spots[1].detectionCount == 0 #available spot



def black_car_test():
    cap = cv2.VideoCapture()
    carSpot.imageWidth=int(1280)
    carSpot.imageHeight=int(720)
    #insert CarSpotColorSample Region

    #BlackCarSpot
    carSpot(pts=np.array([[494,349],[521,349],[583,386],[556,391]]),carSpotID=1,takenThreshold=0,isTaken=False)

    '''
def reflection_test1():
    cap = cv2.VideoCapture('./testSamples/Sparce.png')
'''
'''
Reflection Region #1 [Long zone]
[572,364],[594,391],[654,395],[636,400]
Reflection Region #2 [Long]
[898,436],[907,433],[957,459],[947,463]


<<<<<<< HEAD
=======


def black_car_test():
    cap = cv2.VideoCapture()
    carSpot.imageWidth=int(1280)
    carSpot.imageHeight=int(720)
    #insert CarSpotColorSample Region

    #BlackCarSpot
    carSpot(pts=np.array([[494,349],[521,349],[583,386],[556,391]]),carSpotID=1,takenThreshold=0,isTaken=False)


def reflection_test1():
        cap = cv2.VideoCapture()

'''
'''
Reflection Region #1 [Long zone]
[572,364],[594,391],[654,395],[636,400]
Reflection Region #2 [Long]
[898,436],[907,433],[957,459],[947,463]


>>>>>>> origin
CarSpot #1 [Short zone]
[668,381],[683,382],[683,395],[667,396]

CarSpot #1 [Long zone]
[606,363],[627,360],[685,393],[667,396]

Carspot #2 [Long zone]
[911,432],[925,425],[967,451],[960,457]

Carspot #3 Always true [Short Zone]
[694,380],[709,378],[708,391],[694,392]
'''
'''
def reflection_test2():
        cap = cv2.VideoCapture()
'''
'''
Reflection Region #1 [Long zone]
[572,364],[594,391],[654,395],[636,400]
Reflection Region #2 [Long]
[898,436],[907,433],[957,459],[947,463]


CarSpot #1 [Short zone]
[668,381],[683,382],[683,395],[667,396]

CarSpot #1 [Long zone]
[606,363],[627,360],[685,393],[667,396]

Carspot #2 [Long zone]
[911,432],[925,425],[967,451],[960,457]

Carspot #3 Always true [Short Zone]
[694,380],[709,378],[708,391],[694,392]
'''
