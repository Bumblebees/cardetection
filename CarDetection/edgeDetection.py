'''
Description: edgeDetection.py Takes control of functionality relation to edge detection with the system.
Author: Caleb Stott
Revision: 16/10/2015
'''
import numpy as np
import math
import pickle
import cv2
import matplotlib.path as mplPath
import time

DEBUG=True

defaultEdgeThreshold=200
defualtWeight=2

class edgeDetection:
    cannyImage=np.zeros((1,1,1), np.uint8)#for some reason its (y, x, colorDepth)

    def __init__(self, edgeThreshold=defaultEdgeThreshold,weight=defualtWeight):
        self.edgesWithinSpot=0#store number of edges found in spot in last frame
        self.edgeThreshold=edgeThreshold
        self.weight=weight

    #Sets image to a black image with edge highlighted in white.
    @staticmethod
    def setImage(image):
        edgeDetection.cannyImage=cv2.Canny(image,100,200)#Todo name last two arguments in canney, Lower and Upper Threshhold controls for Canny respectively
        if(DEBUG):
            cv2.imshow('DEBUG',edgeDetection.cannyImage)

    #calculates number of edge points within image
    def analyse(self,mask=0):
        maskedImage=cv2.bitwise_and(edgeDetection.cannyImage,edgeDetection.cannyImage,mask=mask)
        self.edgesWithinSpot=cv2.countNonZero(maskedImage)
        #if(DEBUG):
           # print(self.edgesWithinSpot)

    #tests the ammount of edges against the threshhold for edge image detection component.
    def hasDetected(self):
        if(self.edgesWithinSpot>self.edgeThreshold):
            return True
        else:
            return False

    def __str__(self):
        '''give debug information about detection'''
        return 'ePts:'+str(self.edgesWithinSpot)+'\neThr:'+str(self.edgeThreshold)

