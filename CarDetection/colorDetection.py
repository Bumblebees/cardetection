''' 
Description: ColorDetection.py contains the functionality corresponding to the color analysis component of the image detection.
Author: Caleb Stott
Revision: 16/10/2015
'''
import numpy as np
import math
import pickle
import cv2
import matplotlib.path as mplPath
from carSpot import *

defaultColorDifferenceThresholdSum=40
defaultColorDifferenceThreshold=[10.0,10.0,10.0,0]

class colorDetection:

    def __init__(self):
        #self.colorDifferenceThreshold=colorDifferenceThreshold #allow individual thresholds for spots
        #if carSpot.useSumOfColorDifference:
        #    self.colorDifferenceThreshold=defaultColorDifferenceThresholdSum
        #else:
        self.colorDifferenceThreshold=defaultColorDifferenceThreshold
        self.differenceFromReference=0#store the last calulated difference to display
        #self.referenceSpot=


        carSpot(np.array([[200,200],[200,300],[400,300],[400,200]]),carSpotID=0,takenThreshold=0,isTaken=False)#reference spots for the colours
        self.weight=1
    #Looks at the difference between the Reference zone (Region of road without a car in it) and the Car Region
    def analyse(self,image,mask=0):
        self.differenceFromReference= np.subtract(cv2.mean(image,referenceSpot.mask),cv2.mean(image,self.mask))
    #Updates the mask that is analyised for color means that overlaps the Car Region.
    def reDrawMask(self):
        self.referenceMask = np.copy(carSpot.BlankMask)
        return cv2.drawContours(self.mask, [self.polypts], -1, 255, -1)
    #Checks to see if the CarSpot passes the Color test.
    def hasDetected(self):
        '''
        if carSpot.useSumOfColorDifference:
            #SUM OF DIFFERENCES
            if(cs.differenceFromReference>cs.colorDifferenceThreshold):
                noDifferenceInSpot=False
                if cs.detectionCount<=carSpot.holdingWeight*cs.takenThreshold:#to avoid overflow and to reduce time taken to update when spot available
                        cs.detectionCount+=carSpot.colourWeight
        else:'''
        #INDIVIDUAL COLOUR DIFFERENCE
        if (abs(cs.differenceFromReference[0])>cs.colorDifferenceThreshold[0]
        or abs(cs.differenceFromReference[1])>cs.colorDifferenceThreshold[1]
        or abs(cs.differenceFromReference[2])>cs.colorDifferenceThreshold[2]):
            return True

        return False

    def __str__(self):
        return 'BGRThr: '+str(np.array(cs.colorDetector.colorDifferenceThreshold))+'\nBGRDIFF: '+ str(np.array(self.differenceFromReference).astype(int))
