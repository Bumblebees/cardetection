''' 
Description: carSpots.py TrackbarSettings for Blob Detection (Not Longer in Use)
Author: Caleb Stott
Revision: 10/8/2015
'''
import cv2
import numpy as np

def loadSettingsFromTrackbars(x):#function for trackbar update
    #setup Blob detector
    params=cv2.SimpleBlobDetector_Params()
   # print (params.thresholdStep)
    params.blobColor = cv2.getTrackbarPos('blobColor', 'Settings')
    params.minThreshold = cv2.getTrackbarPos('minThreshold', 'Settings')
    params.thresholdStep = cv2.getTrackbarPos('thresholdStep', 'Settings')
    params.maxThreshold = cv2.getTrackbarPos('maxThreshold', 'Settings')

    params.minDistBetweenBlobs=cv2.getTrackbarPos('minDistBetweenBlobs', 'Settings')

    params.filterByArea= cv2.getTrackbarPos('filterByArea', 'Settings')
    params.minArea = cv2.getTrackbarPos('minArea', 'Settings')
    params.maxArea = cv2.getTrackbarPos('maxArea', 'Settings')

    params.filterByCircularity = cv2.getTrackbarPos('filterByCircularity', 'Settings')
    params.minCircularity = cv2.getTrackbarPos('minCircularity', 'Settings')/100 #circularity of a square is 0.785
    params.maxCircularity = cv2.getTrackbarPos('maxCircularity', 'Settings')/100

    params.filterByConvexity = cv2.getTrackbarPos('filterByConvexity', 'Settings')
    params.minConvexity = cv2.getTrackbarPos('minConvexity', 'Settings')/100 #perfect circle is 1

    params.filterByInertia = cv2.getTrackbarPos('filterByInertia', 'Settings')
    params.maxInertiaRatio = cv2.getTrackbarPos('maxInertiaRatio', 'Settings')/100
    params.minInertiaRatio = cv2.getTrackbarPos('minInertiaRatio', 'Settings')/100

    #print (dir(params))
    #print (detector)
    return params
    #global detector
    detector=cv2.SimpleBlobDetector_create(params)
    #print (detector)
    #return params

def createTrackbars():
    cv2.imshow('Settings', np.zeros((480,480,3), np.uint8))
    #cv2.createButton("button",callbackButton, NULL,CV_CHECKBOX,0)
    cv2.createTrackbar('blobColor','Settings',10,255,loadSettingsFromTrackbars)
    cv2.createTrackbar('minThreshold','Settings',10,255,loadSettingsFromTrackbars)
    cv2.createTrackbar('thresholdStep','Settings',10,255,loadSettingsFromTrackbars)#do not set to 0
    cv2.createTrackbar('maxThreshold','Settings',200,255,loadSettingsFromTrackbars)
    cv2.createTrackbar('minDistBetweenBlobs','Settings',50,255,loadSettingsFromTrackbars)
    cv2.createTrackbar('filterByArea','Settings',1,1,loadSettingsFromTrackbars)#refer to size of carspot object
    cv2.createTrackbar('minArea','Settings',1500,10000,loadSettingsFromTrackbars)
    cv2.createTrackbar('maxArea','Settings',3000,2000000,loadSettingsFromTrackbars)
    cv2.createTrackbar('filterByCirculartiy','Settings',1,1,loadSettingsFromTrackbars)
    cv2.createTrackbar('minCircularity','Settings',65,100,loadSettingsFromTrackbars)
    cv2.createTrackbar('maxCircularity','Settings',95,100,loadSettingsFromTrackbars)
    cv2.createTrackbar('filterByConvexity','Settings',1,1,loadSettingsFromTrackbars)
    cv2.createTrackbar('minConvexity','Settings',87,100,loadSettingsFromTrackbars)
    cv2.createTrackbar('filterByInertia','Settings',1,1,loadSettingsFromTrackbars)
    cv2.createTrackbar('minInertiaRatio','Settings',25,100,loadSettingsFromTrackbars)
    cv2.createTrackbar('maxInertiaRatio','Settings',75,100,loadSettingsFromTrackbars)

    loadSettingsFromTrackbars(1)#load the default settings for trackbar into the detector
'''settings
'blobColor', 'filterByArea', 'filterByCircularity',
'filterByColor', 'filterByConvexity', 'filterByInertia',
'maxArea', 'maxCircularity', 'maxConvexity', 'maxInertiaRatio',
'maxThreshold', 'minArea', 'minCircularity', 'minConvexity',
'minDistBetweenBlobs', 'minInertiaRatio', 'minRepeatability',
'minThreshold', 'thresholdStep
'''
