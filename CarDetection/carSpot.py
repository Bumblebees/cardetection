'''
Description: carSpots.py defines carspot regions within the camera's frame and stores detection attributes to be used in image detection, controls threshholds and important details for carpark regions.
Defines functionality for detecting cars.
Author: Caleb Stott
Revision: 16/10/2015
'''
import numpy as np
import math
import pickle
import cv2
import matplotlib.path as mplPath
from carDatabase import *
from edgeDetection import *
#from colorDetection import *

DEBUG=True

#location when adding a first spot. [0,0] is top left corner
initialx=50
initialy=50

#settings to configure where new spots are added in getNewSpotLocation Fuction
NewSpotxSpacing=10
NewSpotySpacing=100
NewSpotxSize=100
NewSpotySize=200
spotsPerRow=10

#how big the clickable area around a spot is in pixels
pointSelectSize=15

#variable for mouse handler to store selected car spot object
selectedPolyPoint=[0,-1]#list carspot object and reference index of polygon or if index =-2 list object and its current corordinates when being moved

class carSpot:
    '''Defines where a carspot region is within the cameras field of view and stores all the detection attributes'''


    #static class variables (shared for each instance of carSpot
    spots=[] #array containing all spots (TODO: move to separate class that handles all the spots)

    #variables to store the size of the image (used to create a binary image mask for each spot)
    #first spot created needs to specify these
    imageWidth=1
    imageHeight=1

    #mask image template, needs to be initialised to size of image
    #individual spots edit a copy of this to create their own mask
    BlankMask=np.zeros((1,1,1), np.uint8)

    #Variables to change how the system filters out noise
    reductionWeight=2#how quickly the spot becomes available after a car leaves
    holdingWeight=2#how long the spot will hold its state (multiples of takenThreshold)

    #region Constructor
    def __init__(self, pts,                     #polygon points defining the region of the spot
                 carSpotID,                     #Id of the spot in the database
                 takenThreshold,                #Value that detectionCount must pass in order to register as taken
                 isTaken=False,                 #set False by default
                 imageWidth=0, imageHeight=0):  #Image size is not changed when no value given

        if(imageWidth>0 or imageHeight>0):
            carSpot.imageWidth=imageWidth
            carSpot.imageHeight=imageHeight
        else:
            #check they have been previously defined
            if(carSpot.imageWidth==1 or carSpot.imageHeight==1):
                print('Warning: Imagesize has not been initialised in car Spot class. System will not work Properly')

        #Assign instance variables
        self.polypts=pts  #Bounding polygon
        self.takenThreshold=takenThreshold
        self.carSpotID=carSpotID#need to check whether spot is already assigned
        self.isTaken=isTaken
        self.detectionCount=0

        #get the Row ID (not nessisary, used mainly for double checking
        self.updateRowID()

        #initialise the blank mask
        #mask is binary image defining region of spot, used by algorithms to easily extract the region from an image
        carSpot.BlankMask=np.zeros((carSpot.imageHeight,carSpot.imageWidth,1), np.uint8)#for some reason its (y, x, colorDepth)
        self.reDrawMask()

        #initilise Detection objects
        #self.colorDetector=colorDetection()
        self.edgeDetector=edgeDetection()

        carDatabase.NodeID=1
        carDatabase.updateSpotAssignedNode(carSpotID)
        carSpot.spots.append(self)#add spot to the list of car Spots

    @staticmethod
    def addPrompt():
        spotID=input('Enter Spot ID(Last Spot was ' + str(carSpot.spots[-1].carSpotID) + '): ')
        x=int(input('Enter x coordinate: '))
        y=int(input('Enter y coordinate: '))
        carSpot(carSpot.pointsFromCoordinates(x,y),takenThreshold=10,carSpotID=spotID)
        carSpot.spots[-1].edgeDetector.edgeThreshold=int(input('Enter edge Threshold: '))

    @staticmethod
    def editPrompt():
        spotID=int(input('Enter Spot ID to edit: '))
        spotToEdit=None
        for cs in carSpot.spots:
            if(spotID==cs.carSpotID):
                spotToEdit=cs
        if(spotToEdit!=None):
            spotToEdit.edgeDetector.edgeThreshold=int(input('Enter edge Threshold: '))
            spotToEdit.edgeDetector.edgeWeight=int(input('Enter edge Weight: '))
        else:
            print('no spots found with id: ' +spotID)


    @staticmethod
    def displayPrompt():
        spotID=int(input('Enter Spot ID to display info: '))
        for cs in carSpot.spots:
            if(spotID==cs.carSpotID):
                cs.display()

    @staticmethod
    def displayAll():
        for cs in carSpot.spots:
            cs.display()
    def display(self):
        print(self)

    #endregion

    #region Detection

    def resetSpot(self):
        '''resets detection statistics for a spot'''
        self.isTaken=False
        self.detectionCount=0
        self.updateRowID()
        #self.colorDetector=colorDetection()

        ew=self.edgeDetector.weight
        et=self.edgeDetector.edgeThreshold
        self.edgeDetector=edgeDetection(et,ew)

        carDatabase.updateSpotAssignedNode(self.carSpotID)
        self.reDrawMask()
    @staticmethod
    def resetAllSpots():
        '''resets detection statistics for all spots'''
        print(carSpot.imageHeight,carSpot.imageWidth)
        carSpot.BlankMask=np.zeros((carSpot.imageHeight,carSpot.imageWidth,1), np.uint8)
        for cs in carSpot.spots:
            cs.resetSpot()

    @staticmethod
    def analyseSpots(image):#looks at the spots and updates its attributes

        edgeDetection.setImage(image)
        for cs in carSpot.spots:
            if (cs.edgeDetector.weight!=0):
                cs.edgeDetector.analyse(cs.mask)
        '''TODO: Fix Colour Detection
        if (carSpot.colourDetector.weight!=0):
            for cs in carSpot.spots:
                colorDetector.analyse(image,cs.mask)
        '''

    def updateEdgeThreshold(self):#Updates Edge threshhold based on carspot area
        self.edgeDetector.edgeThreshold= defaultEdgeThreshold * 1000 / self.getSpotArea()

    def updateEdgeThreshold(self, newThreshold):#Updates Edge threshhold based on carspot area
        self.edgeDetector.edgeThreshold= newThreshold

    @staticmethod
    def updateAllEdgeThresholds(newThreshold):
        for cs in carSpot.spots:
            cs.updateEdgeThreshold(newThreshold)

    def updateEdgeWeight(self, newWeight):#Updates Edge threshold based on carspot area
        self.edgeDetector.edgeWeight= newWeight

    @staticmethod
    def updateAllEdgeWeight(newWeight):
        for cs in carSpot.spots:
            cs.updateEdgeWeight(newWeight)
    '''
    @staticmethod
    def changeAllColorThresholds(threshold):
        for cs in carSpot.spots:
            cs.colorDetector.colorDifferenceThreshold=threshold
    '''


    @staticmethod
    def updateSpots():#checks the attributes and updates the detectionCount and taken state
        for cs in carSpot.spots:
            #colorDetected=cs.colorDetector.hasDetected()
            edgeDetected=cs.edgeDetector.hasDetected()

            #COLOUR DETECTION
            '''
            if(cs.colorDetector.colourWeight!=0):
                if(colorDetected):
                    if cs.detectionCount<=carSpot.holdingWeight*cs.takenThreshold:#to avoid overflow and to reduce time taken to update when spot available
                            cs.detectionCount+=cs.colorDetector.weight
            '''
            #EDGE DETECTION
            if(cs.edgeDetector.weight!=0):#check if using edge detection
                if(edgeDetected):
                    if cs.detectionCount<=carSpot.holdingWeight*cs.takenThreshold:#to avoid overflow and to reduce time taken to update when spot available
                            cs.detectionCount+=cs.edgeDetector.weight

            #REDUCE COUNT IF NOTHING FOUND
            '''not colorDetected and '''
            if(not edgeDetected and cs.detectionCount!=0):
                cs.detectionCount-=carSpot.reductionWeight
                #print cs.detectionCount

            #CHANGING STATES
            if cs.detectionCount>=cs.takenThreshold:
                if cs.isTaken==False:#update on changing state
                    carDatabase.updateSpot(cs.carSpotID, True)
                cs.isTaken=True
            else:
                if cs.isTaken==True:
                    carDatabase.updateSpot(cs.carSpotID, False)#to update Database
                cs.isTaken=False

    #endregion

    #region Accessor Methods

    def updateRowID(self):#is set to 'NULL' when not part of row or removing spot
        self.rowID=carDatabase.getSpotRow(self.carSpotID)

    def movePoint(self, index, dx, dy):
        '''Moves a point relative to its current position'''
        self.polypts[index]=np.add(self.polypts[index],[dx,dy])
        self.reDrawMask()#update changes to mask
    def setPoint(self, index, x, y):
        '''Sets a point to a absolute position'''
        self.polypts[index]=[x,y]#sets the
        self.reDrawMask()#update changes to mask
    def move(self, dx, dy):#move spot relative
        '''Move Entire Spot relative to its current position'''
        self.polypts=np.add(self.polypts,[[dx,dy],[dx,dy],[dx,dy],[dx,dy]])
        self.reDrawMask()#update changes to mask
    @staticmethod
    def moveAll(dx, dy):
        '''Moves all car spots relative to thier current position'''
        for cs in carSpot.spots:
            cs.move(dx,dy)

    def reDrawMask(self):
        '''Updates the Mask stored for each spot'''
        self.mask = np.copy(carSpot.BlankMask)
        self.mask = cv2.drawContours(self.mask, [self.polypts], -1, 255, -1)
    #endregion

    #region Interfacing Controls
    @staticmethod
    def on_mouse(event,x,y,flag,param):
        ''' Mouse handler. Handles how the spots are moved on the screen'''
        carSpot.mouse=(x,y)
        global selectedPolyPoint #treat this as a global variable and not local
        if(event == cv2.EVENT_LBUTTONDOWN):
            for cs in carSpot.spots:
                if carSpot.point_in_poly(x,y,cs.polypts):
                    #mouse within region of a carSpot
                    selectedPolyPoint=[cs,-2,[x,y]]#mark current carSpot to move and where the mouse was clicked
                for n in range(0,len(cs.polypts)):#for each point in the polygon
                    if (x<(cs.polypts[n][0]+pointSelectSize) and
                        x>(cs.polypts[n][0]-pointSelectSize) and
                        y<(cs.polypts[n][1]+pointSelectSize) and
                        y>(cs.polypts[n][1]-pointSelectSize)
                        ):
                        #mouse within clickable region for that point
                        selectedPolyPoint=[cs,n]#mark current carspot and point in polygon as selected point
                        #print (selectedPolyPoint)#debug

        elif(event == cv2.EVENT_MOUSEMOVE):
            #TODO:Redraw as mouse is moving
            #print (selectedPolyPoint)#debug
            if selectedPolyPoint[1]==-2:
                #move the entire spot
                x2,y2=selectedPolyPoint[2]
                selectedPolyPoint[2]=(x,y)#update previous mouse position to current position
                selectedPolyPoint[0].move(x-x2,y-y2)
            elif selectedPolyPoint[1]>=0:
                #move a specific point
                selectedPolyPoint[0].setPoint(selectedPolyPoint[1],x,y)

        elif (event == cv2.EVENT_LBUTTONUP):

            #reset selected car spot so subsequent clicks dont move again
            selectedPolyPoint=[0,-1]

    #define trackbar in settings window
    #cv2.createTrackbar(trackbarName, windowName, init Value, max Value, onChange(new value) )
    @staticmethod
    def createTrackbars():
         #icon=cv2.imread('appicon.png',cv2.IMREAD_UNCHANGED)
        #icon=cv2.resize(icon,0,fx=0.25,fy=0.25,interpolation=cv2.INTER_AREA)
        cv2.imshow('Settings', np.zeros((1,1,1), np.uint8))
        #cv2.createButton("button",callbackButton, NULL,CV_CHECKBOX,0)
        #cv2.createTrackbar('colourWeight','Settings',carSpot.colourWeight,255,carSpot.loadSettingsFromTrackbars)
        #cv2.createTrackbar('edgeWeight','Settings',carSpot.edgeWeight,255,carSpot.loadSettingsFromTrackbars)
        cv2.createTrackbar('holdingWeight','Settings',carSpot.holdingWeight,255,carSpot.loadSettingsFromTrackbars)#do not set to 0
        cv2.createTrackbar('reductionWeight','Settings',carSpot.reductionWeight,255,carSpot.loadSettingsFromTrackbars)
        #cv2.createTrackbar('useSumOfColorDifference','Settings',carSpot.useSumOfColorDifference,1,carSpot.loadSettingsFromTrackbars)
        #if(carSpot.useSumOfColorDifference):
        #    cv2.createTrackbar('colorDifferenceThreshold','Settings',defaultColorDifferenceThresholdSum,255,carSpot.loadSettingsFromTrackbars)
        #else:
        '''
        cv2.createTrackbar('colorDifferenceThresholdBlue','Settings',int(defaultColorDifferenceThreshold[0]),255,carSpot.loadSettingsFromTrackbars)
        cv2.createTrackbar('colorDifferenceThresholdGreen','Settings',int(defaultColorDifferenceThreshold[1]),255,carSpot.loadSettingsFromTrackbars)
        cv2.createTrackbar('colorDifferenceThresholdRed','Settings',int(defaultColorDifferenceThreshold[2]),255,carSpot.loadSettingsFromTrackbars)
        '''
        cv2.createTrackbar('edgeThreshold','Settings',defaultEdgeThreshold,7000,carSpot.updateAllEdgeThresholds)
        cv2.createTrackbar('edgeWeight','Settings',1,100,carSpot.updateAllEdgeWeight)

    @staticmethod
    def loadSettingsFromTrackbars(x):
        '''Handles changes to trackbars where x is the trackbar new position'''
        #carSpot.colourWeight = cv2.getTrackbarPos('colourWeight', 'Settings')
        #carSpot.edgeWeight = cv2.getTrackbarPos('edgeWeight', 'Settings')

        carSpot.holdingWeight = cv2.getTrackbarPos('holdingWeight', 'Settings')
        carSpot.reductionWeight = cv2.getTrackbarPos('reductionWeight', 'Settings')
        '''TODO: Move individual trackbars to each spot detector
        if ((cv2.getTrackbarPos('useSumOfColorDifference', 'Settings')==1)
        and carSpot.useSumOfColorDifference==False):
            carSpot.useSumOfColorDifference = True
            cv2.destroyWindow('Settings')
            carSpot.createTrackbars()
            carSpot.changeAllColorThresholds(cv2.getTrackbarPos('colorDifferenceThreshold', 'Settings'))

        elif ((cv2.getTrackbarPos('useSumOfColorDifference', 'Settings'))==0
        and carSpot.useSumOfColorDifference==True):
            carSpot.useSumOfColorDifference == False
            cv2.destroyWindow('Settings')
            carSpot.createTrackbars()
        '''
        '''
        carSpot.changeAllColorThresholds(   [cv2.getTrackbarPos('colorDifferenceThresholdBlue', 'Settings'),
                                            cv2.getTrackbarPos('colorDifferenceThresholdGreen', 'Settings'),
                                            cv2.getTrackbarPos('colorDifferenceThresholdRed', 'Settings'),
                                            0])
        '''
    @staticmethod
    def storeSpots(filename):
        '''Stores a binary representation of the spots to file.
        !!! Caution: files become incompatible if changes are make to carSpot class !!!'''
        pickle.dump(carSpot.spots, open(filename, "wb"))
    def loadSpots(filename):
        '''Loads spots from file'''
        carSpot.spots=pickle.load(open(filename, "rb"))
        carSpot.resetAllSpots()

    #endregion

    #region drawing Methods


    def __str__(self):
        '''give debug information about spot'''
        text ='ID: '+str(self.carSpotID)+'\n'
        text+='Row: '+str(self.rowID)+'\n'
        text+='Area: '+str(int(self.getSpotArea()))+'\n'
        #text+=str(self.colorDetector)+'\n'
        text+=str(self.edgeDetector)+'\n'
        text+=str(self.polypts)#+'\n'
        return text

    @staticmethod
    def drawSpots(image):
        '''Draws the regions on the image and whether they are taken or not. plus debug info'''
        for cs in carSpot.spots:
            #Draw outline of spot depending on state
            if cs.isTaken:
                cv2.polylines(image, [cs.polypts], True, (0,255,0), 3)#draw green
            else:
                cv2.polylines(image, [cs.polypts], True, (0,0,255), 3)#draw red

            #TODO: Draw Color detection Reference for each spot
            #cv2.polylines(image, [cs.colorDetector.referenceSpot.polypts], True, (255,0,0), 3)
            #write debug information (can get very cluttered. TODO: write info to second window and only display id on spots)
            text ='\nID: '+str(cs.carSpotID)+'\n'
            #text+='Row: '+str(cs.rowID)+'\n'
            if(DEBUG):
                #text+='Area: '+str(int(cs.getSpotArea()))+'\n'
                #text+=str(cs.colorDetector)+'\n'
                text+=str(cs.edgeDetector)+'\n'
                #text+=str(cs.polypts)#+'\n'

            #display coordinates of points in each polygon
                #for pt in cs.polypts:
                #    cv2.putText(image, '('+str(pt[0])+','+str(pt[1])+')' , (pt[0],pt[1]),cv2.FONT_HERSHEY_SIMPLEX,0.4,(0.0,0.0,0.0),2)

            x0, y0, dy = cs.polypts[0][0], cs.polypts[0][1], 15 #starting positions and vertical spacing

            #mouse action drawing
            if selectedPolyPoint[1]>=0:#point selected
                #draw selected point for mouse
                x,y=selectedPolyPoint[0].polypts[selectedPolyPoint[1]]
                cv2.circle(image,(x,y),pointSelectSize,(0,127,255), thickness=-1)#draw orange [selectedPolyPoint[0].polypts], True,
            elif selectedPolyPoint[1]==-2 : #spot selected
                cv2.polylines(image, [selectedPolyPoint[0].polypts], True, (0,127,255), 3)#draw green


            image=carSpot.drawText(x0,y0,text, image)
        return image

    @staticmethod
    def drawText(x0,y0,text,image,dy=15):
        '''Writes multiline text strings to an image'''
        for i, line in enumerate(text.split('\n')):
            #cv2.putText doesn't handle newline characters, so manually put text for each line
            y = y0 + i*dy
            cv2.putText(image, line, (x0, y ), cv2.FONT_HERSHEY_SIMPLEX, 0.4,(0.0,0.0,0.0),2)
        return image
    #endregion

    #region polygon helper functions
    @staticmethod
    def getNewSpotLocation():#to avoid creating new spots on top of one another
        n=len(carSpot.spots)

        return np.add([[initialx,initialy],[initialx,initialy+NewSpotySize],[initialx+NewSpotxSize,initialy+NewSpotySize],[initialx+NewSpotxSize,initialy]],
                       [[(n%spotsPerRow)*(NewSpotxSize+NewSpotxSpacing),math.floor(n/spotsPerRow)*(NewSpotySpacing+NewSpotySize)],
                        [(n%spotsPerRow)*(NewSpotxSize+NewSpotxSpacing),math.floor(n/spotsPerRow)*(NewSpotySpacing+NewSpotySize)],
                        [(n%spotsPerRow)*(NewSpotxSize+NewSpotxSpacing),math.floor(n/spotsPerRow)*(NewSpotySpacing+NewSpotySize)],
                        [(n%spotsPerRow)*(NewSpotxSize+NewSpotxSpacing),math.floor(n/spotsPerRow)*(NewSpotySpacing+NewSpotySize)]]
                        )
    @staticmethod
    def pointsFromCoordinates(x, y):#get rectangle points from given coordinate
        return np.array([[x,y],[x,y+NewSpotySize],[x+NewSpotxSize,y+NewSpotySize],[x+NewSpotxSize,y]])

    @staticmethod
    def point_in_poly(x, y, poly):#function to detect whether a point is within a spot
        bbPath = mplPath.Path(np.array(poly))
        return bbPath.contains_point((x, y))
    def pointInSpot(self, pt):#function to detect whether a point is within a spot
        bbPath = mplPath.Path(np.array(self.polypts))
        return bbPath.contains_point(pt)
    def polyInSpot(self, poly):#function to detect whether a polygon is within a spot
        bbPath = mplPath.Path(np.array(self.polypts))
        containsPoints=False
        boolArray=bbPath.contains_points(poly)
        for bo in boolArray:
            if bo==True:
                containsPoints=True
        return containsPoints

    def getSpotArea(self):
        #vectors
        a = length([ self.polypts[1][0]-self.polypts[0][0],self.polypts[1][1]-self.polypts[0][1] ])#left hand edge
        b = length([ self.polypts[2][0]-self.polypts[1][0],self.polypts[2][1]-self.polypts[1][1] ])#botton edge
        c = length([ self.polypts[3][0]-self.polypts[2][0],self.polypts[3][1]-self.polypts[2][1] ])#right edge
        d = length([ self.polypts[0][0]-self.polypts[3][0],self.polypts[0][1]-self.polypts[3][1] ])#top edge
        e = length([ self.polypts[2][0]-self.polypts[0][0],self.polypts[2][1]-self.polypts[0][1] ])#diagonal
        #Heron's Formula(using 2 triangles)
        p1=a+b+e
        p1=p1/2
        a1=math.sqrt(p1*(p1-a)*(p1-b)*(p1-e))
        p2=c+d+e
        p2=p2/2
        a2=math.sqrt(p1*(p1-c)*(p1-d)*(p1-e))
        #print(p1,p2, a1, a2)
        return a1+a2


#Generalised Vector Math Stuff
def dotproduct(v1, v2):
    return sum((a*b) for a, b in zip(v1, v2))

def length(v): #calculates length of a vector
    return math.sqrt(v[0]*v[0]+v[1]*v[1]) #dotproduct(v, v))#

def length(v):
    return math.sqrt(v[0]*v[0]+v[1]*v[1]) #dotproduct(v, v))

def angle(v1, v2):
    return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))

#endregion
