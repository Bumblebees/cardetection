''' 
Description: CarDatabase.py Control functions and Connection interface to the database.
Author: Caleb Stott
Revision: 16/10/2015
'''
import pymysql as mdb

'''Database Settings'''
DBhostname='latcs7.cs.latrobe.edu.au'
#DBhostname='hostname.cs.latrobe.edu.au/'
DBusername='15pra02'
DBpassword='THEbumblebees2015'
DB='15pra02'

class carDatabase:

    #database functions
    NodeID=1 #unique identifier for each node

    #Tests the connection to the database.
    def testConnection():
        try:
            db = mdb.connect(DBhostname, DBusername, DBpassword,DB)#host='latcs7.cs.latrobe.edu.au', user='15pra02', passwd='THEbumblebees2015', db='15pra02')
            print ('Successfully connected to database')
            db.close()
        except mdb.err.OperationalError as e:
            print (e)
            print ('Cannont Connect to database')

    #Retrieves What Row that CarparkID is assigned to
    @staticmethod
    def getSpotRow(SpotID):
        try:
            db = mdb.connect(DBhostname, DBusername, DBpassword,DB)
            cursor=db.cursor()
            cursor.execute("SELECT carRowID FROM carSpot WHERE carSpotID='%s'" % (SpotID))
            data = cursor.fetchall()

            cursor.close()
            db.commit()
            db.close()

            if(data!=()):
                return data[0][0]
            else:
                return 'NULL'
        except mdb.err.OperationalError as e:
            print (e)
            print ('Could not Retrieve Row info')
            return 'NULL'

    #updates the spot in the database to indicate that the current node is updating it
    @staticmethod
    def updateSpotAssignedNode(SpotID):
        try:
            db = mdb.connect(DBhostname, DBusername, DBpassword,DB)
            cursor=db.cursor()
            cursor.execute("UPDATE carSpot SET AssignedNode='%s' WHERE carSpotID='%s'" % (carDatabase.NodeID,SpotID))

            cursor.close()
            db.commit()
            db.close()
        except mdb.err.OperationalError as e:
            print (e)
            print ('Database was not updated - Spot not assigned to node')

    #set whether the spot is taken or not in the database
    @staticmethod
    def updateSpot(SpotID, isTaken):
        try:
            db = mdb.connect(DBhostname, DBusername, DBpassword,DB)
            cursor=db.cursor()
            if isTaken==True:
                cursor.execute("UPDATE carSpot SET isTaken='Y' WHERE carSpotID='%s'" % (SpotID))
                #cursor.execute("UPDATE carSpot SET Assignednode='%s' WHERE carSpotID='%s'" % (carDatabase.NodeID,SpotID))
            else:
                cursor.execute("UPDATE carSpot SET isTaken='N' WHERE carSpotID='%s'" % (SpotID))

            cursor.close()
            db.commit()
            db.close()
        except mdb.err.OperationalError as e:
            print (e)
            print ('Database was not updated')

    #get current spot status
    @staticmethod
    def getSpotStatus(SpotID): #not really needed. would only be used to double check
        try:
            db = mdb.connect(DBhostname, DBusername, DBpassword,DB)
            cursor=db.cursor()
            cursor.execute("SELECT isTaken FROM carSpot WHERE carSpotID='%s'" % (SpotID))
            data = cursor.fetchall()

            cursor.close()
            db.commit()
            db.close()
            if data[0][0]=='Y':
                return True
        except mdb.err.OperationalError as e:
            print (e)
            print ('Could not retrieve status')
        return False

