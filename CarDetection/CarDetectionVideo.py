'''
Description: CarDetectionVideo.py is the TopLevel for image analysis for video feed. It calls functionality from carSpots.py to run detection and has code to control the setting up of carRegions.
Author: Caleb Stott
Revision: 16/10/2015
'''
import time
import os
import traceback
import cv2
from carSpot import *

#Create display window and assign mouse delegate
cv2.namedWindow('Frame', cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback("Frame", carSpot.on_mouse)

SpotsFileName='bin'#extension type

#do we want to open a web stream
webStream=False
while(True):
    answer=input('Would you like to view a web Stream?(y/n)').lower()
    if(answer=='y'):
        webStream=True
        break
    elif(answer=='n'):
        break

#do we want to save display to file
saveVideo=False
while(True):
    answer=input('Would you like to save display to file?(y/n) ').lower()
    if(answer=='y'):
        saveVideo=True
        break
    elif(answer=='n'):
        break

#create video capture object
if(webStream):
    webAddress=input('Enter Hostname/IP Address of Stream: ')
    try:
        cap = cv2.VideoCapture('http://'+webAddress+':8080/video?dummy=video.mjpg')
        videoFile=webAddress
    except Exception as e:
        raise
else:
    videoFile=""
    while(videoFile==""):
        videoFile = input('Enter file to run(Image, Image sequence or Video): ')
        if(os.path.isfile(videoFile)):
            try:
                cap = cv2.VideoCapture(videoFile)
            except Exception as e:
                raise
        else:
            videoFile=""
            print("File not found")




fps=cap.get(cv2.CAP_PROP_FPS)
width=int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
height=int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

print (width, height)#print resolution to confirm that file has been loaded
print('FPS: ',fps)
carSpot.imageHeight, carSpot.imageWidth=height,width
cap.set(cv2.CAP_PROP_POS_FRAMES, 1.0)#set capture object to first frame
cv2.resizeWindow('Frame', width, height)


if(not fps>0):
    fps=20#set default fps
    print('FPS: ',fps)

if(saveVideo):
    #create video writer object
    saveFileLocation=input('Enter filename to save to:')
    try:
        out = cv2.VideoWriter(saveFileLocation+'.avi',cv2.VideoWriter_fourcc('X','2','6','4'), fps, (width, height))
    except Exception as e:
        #TODO:ADD exception handling
        raise

pauseDetection=False

try:#loading spots from file
    carSpot.loadSpots('./spotSaves/'+videoFile.split("/")[-1]+'.'+SpotsFileName)
except:#if cant then ask how many
    traceback.print_exc()
    numberOfSpots=int(input('Enter the number of spots to make: '))
    idStartingPoint=1 #input('Enter the ID of the firstSpot:')
    for i in range(1,numberOfSpots+1):
        carSpot(carSpot.getNewSpotLocation(),takenThreshold=10,carSpotID=i,imageWidth=width, imageHeight=height)

carSpot.createTrackbars()

while(cap.isOpened()):
    try:
        #load frame
        ret, image = cap.read()

        #time.sleep(0.1)

        #run algorithms and update spots
        if(not pauseDetection):
            carSpot.analyseSpots(image)#run detection algorithms and update parameters
            carSpot.updateSpots()#check taken status for each spot

        #draw debug info on image
        image=carSpot.drawSpots(image)

        #add current frame count for file
        cv2.putText(image,'Frame: '+str(int(cap.get(cv2.CAP_PROP_POS_FRAMES))),(100,100),cv2.FONT_HERSHEY_SIMPLEX,2,(0.0,0.0,0.0),4)

        #display image
        cv2.imshow('Frame', image)

        if(saveVideo):
            out.write(image)

        #cap.set(cv2.CAP_PROP_POS_FRAMES,cap.get(cv2.CAP_PROP_POS_FRAMES)+fps)#1fps for performance reasons

    except Exception as e: #catch exception thrown when reaching end of file
        #restart video
        if(webStream):
            cap = cv2.VideoCapture('http://'+webAddress+':8080/video?dummy=video.mjpg')
        else:
            cap = cv2.VideoCapture(videoFile)
            saveVideo=False#only need to save file once
        traceback.print_exc()
        print(e)


    #interface keys
    key=cv2.waitKey(20) & 0xFF #get keypress

    try:#so program doesn't crash with invalid input
        if key==ord("q"):
            cv2.destroyAllWindows()
            break
        elif key==ord("2"):
            carSpot.moveAll(0,10)
        elif key==ord("a"):
            carSpot.addPrompt()
        elif key==ord("e"):
            carSpot.editPrompt()
        elif key==ord("d"):
            carSpot.displayPrompt()
        elif key==ord("p"):
            pauseDetection=not pauseDetection#toggle running detection algorithms
        elif key==ord("6"):
            carSpot.moveAll(10,0)
        elif key==ord("8"):
            carSpot.moveAll(0,-10)
        elif key==ord("4"):
            carSpot.moveAll(-10,0)
        elif key==ord("s"):
            carSpot.storeSpots('./spotSaves/'+videoFile.split("/")[-1]+'.'+SpotsFileName)
            print('Spots saved to ./spotSaves/'+videoFile.split("/")[-1]+'.'+SpotsFileName)
        elif key==ord("l"):
            carSpot.loadSpots('./spotSaves/'+videoFile.split("/")[-1]+'.'+SpotsFileName)
        elif key==ord("r"):
            videoFile=input('Enter Video file to test: ')
            cap = cv2.VideoCapture(videoFile)
        elif key==ord("i"):
            newId=int(input('Enter new starting Id for carspots: '))
            for cs in carSpot.spots:
                cs.carSpotID=newId
                newId+=1

        elif key==ord("d"):
            deleteId=int(input('Enter Id of spot to delete: '))
            for cs in carSpot.spots:
                if(cs.carSpotID==deleteId):
                    carSpot.spots.remove(cs)
    except Exception as e:
        print(e)
        traceback.print_exc()

cap.release()
if(saveVideo):
    out.release()
cv2.destroyAllWindows()

