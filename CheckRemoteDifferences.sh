#!/bin/bash
clear

cd ~/cardetection
git remote -v update 
git diff origin/master

while true; do
	read -p "Go ahead with commit?" yn
	case $yn in
		[Yy]* ) git merge origin/master; break;;
		[Nn]* ) exit;;
	* ) echo "Yes or no?";;
	esac
done